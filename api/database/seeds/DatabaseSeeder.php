<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // list comics
       
        \App\Models\Comic::factory()->count(20)->create();        
        // list characters
        \App\Models\Character::factory()->count(20)->create();

        $comics = App\Models\Comic::all();

        App\Models\Character::all()->each(function ($character) use ($comics) {
            $character->comics()->attach(
                $comics->random(rand(1, 2))->pluck('id')->toArray()
            );
        });
    }
}
